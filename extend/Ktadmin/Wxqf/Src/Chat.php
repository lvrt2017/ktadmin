<?php
namespace Ktadmin\Wxqf\Src;

use Ktadmin\Wxqf\Ktadmin;

/**
 * 聊天
 */
class Chat
{
	private $ktadmin;
	private $url;
    private $model = 'ErnieBot'; //要使用的模型
    private $stream = true; //如果设置true则流式输出,false则一次性返回
    private $temperature; //较高的数值会使输出更加随机，而较低的数值会使其更加集中和确定。 默认0.95，范围 (0, 1.0]，不能为0。 建议该参数和top_p只设置1个。 建议top_p和temperature不要同时更改。
    private $top_p; //默认0.8，取值范围 [0, 1.0]。 影响输出文本的多样性，取值越大，生成文本的多样性越强
    private $penalty_score; //通过对已生成的token增加惩罚，减少重复生成的现象。说明： 值越大表示惩罚越大。 默认1.0，取值范围：[1.0, 2.0]。
    private $system; 
    private $stop; 
    private $disable_search; 
    private $enable_citation; 
    private $enable_trace; 
    private $max_output_tokens; 
    private $response_format; 
    private $user_id; 
    private $apiurl; 

    public function __construct(Ktadmin $ktadmin = null)
    {
        $this->ktadmin = $ktadmin;
    }

    /**
     * 初始化配置
     */
    public function initConfig($config)
    {
        
        if(isset($config['model']) && $config['model']){
            $this->model = $config['model'];
        }
        if(@($config['stream'] === 'false' || $config['stream'] === false)){
            $this->stream = false;
        }
        if(isset($config['temperature']) && $config['temperature']){
            $this->temperature = $config['temperature'];
        }
        if(isset($config['top_p']) && $config['top_p']){
            $this->top_p = $config['top_p'];
        }
        if(isset($config['penalty_score']) && $config['penalty_score']){
            $this->penalty_score = $config['penalty_score'];
        }
        if(isset($config['system']) && $config['system']){
            $this->system = $config['system'];
        }
        if(isset($config['stop']) && $config['stop']){
            $this->stop = $config['stop'];
        }
        if(isset($config['disable_search']) && $config['disable_search']){
            $this->disable_search = $config['disable_search'];
        }
        if(isset($config['enable_citation']) && $config['enable_citation']){
            $this->enable_citation = $config['enable_citation'];
        }
        if(isset($config['enable_trace']) && $config['enable_trace']){
            $this->enable_trace = $config['enable_trace'];
        }
        if(isset($config['max_output_tokens']) && $config['max_output_tokens']){
            $this->max_output_tokens = $config['max_output_tokens'];
        }
        if(isset($config['response_format']) && $config['response_format']){
            $this->response_format = $config['response_format'];
        }
        if(isset($config['user_id']) && $config['user_id']){
            $this->user_id = $config['user_id'];
        }
        if(isset($config['apiurl']) && $config['apiurl']){
            $this->apiurl = $config['apiurl'];
        }
        switch ($this->model) {
            case 'ErnieBot':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions";
                break;
            case 'ErnieBot-turbo':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/eb-instant";
                break;
            case 'ErnieBot-turbo-8k':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/ernie_bot_8k";
                break;
            case 'ErnieBot-turbo-4.0':  //ERNIE-4.0-8K
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions_pro";
                break;
            case 'ERNIE-4.0-8K':  //ERNIE-4.0-8K
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions_pro";
                break;
            case 'ERNIE-4.0-8K-Preview': //ERNIE-4.0-8K-Preview
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/ernie-4.0-8k-preview";
                break;
            case 'ERNIE-4.0-8K-Preview-0518': //ERNIE-4.0-8K-Preview
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions_adv_pro";
                break;
            case 'ERNIE-4.0-8K-0329': //ERNIE-4.0-8K-Preview
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/ernie-4.0-8k-0329";
                break;
            case 'ERNIE-4.0-8K-0104':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/ernie-4.0-8k-0104";
                break;
            case 'ERNIE-3.5-8K':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions";
                break;
            case 'ERNIE-3.5-8K-0205':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/ernie-3.5-8k-0205";
                break;
            case 'ERNIE-3.5-8K-1222':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/ernie-3.5-8k-1222";
                break;
            case 'ERNIE-3.5-4K-0205':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/ernie-3.5-4k-0205";
                break;
            case 'ERNIE-3.5-8K-Preview':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/ernie-3.5-8k-preview";
                break;
            case 'ERNIE-3.5-8K-0329':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/ernie-3.5-8k-0329";
                break;
            case 'ERNIE-3.5-128K':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/ernie-3.5-128k";
                break;
            case 'ERNIE-Speed-8K':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/ernie_speed";
                break;
            case 'ERNIE-Speed-128K':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/ernie-speed-128k";
                break;
            case 'ERNIE-Lite-8K-0922':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/eb-instant";
                break;
            case 'ERNIE-Lite-8K-0308':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/ernie-lite-8k";
                break;
            case 'ERNIE-Lite-8K-0725':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/".$this->apiurl;
                break;
            case 'ERNIE-Lite-4K-0704':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/".$this->apiurl;
                break;
            case 'ERNIE-Lite-4K-0516':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/".$this->apiurl;
                break;
            case 'ERNIE-Lite-128K-0419':
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/".$this->apiurl;
                break;
            default:
                $this->url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions";
                break;
        }
    }
    /**
     * 发送聊天
     * @param Array $messages 提问内容，格式[['role'=>'user','content'=>'hello!']]
     * @param Closure $callback 匿名函数
     * @param Array $config 配置信息。 model：要使用的模型的 ID；stream：如果设置true则流式输出,false则一次性返回
     */
    public function sendText($messages = [], $callback, $config = [])
    {
        $this->initConfig($config);
        $postData = [
            'messages' => $messages,
            'stream' => $this->stream
        ];
       if($this->temperature) $postData["temperature"] = $this->temperature;
       if($this->top_p) $postData["top_p"] = $this->top_p;
       if($this->penalty_score) $postData["penalty_score"] = $this->penalty_score;
       if($this->system) $postData["system"] = $this->system;
       if($this->stop) $postData["stop"] = $this->stop;
       if($this->disable_search) $postData["disable_search"] = $this->disable_search;
       if($this->enable_citation) $postData["enable_citation"] = $this->enable_citation;
       if($this->enable_trace) $postData["enable_trace"] = $this->enable_trace;
       if($this->max_output_tokens) $postData["max_output_tokens"] = $this->max_output_tokens;
       if($this->response_format) $postData["response_format"] = $this->response_format;
       if($this->user_id) $postData["user_id"] = $this->user_id;
        
        $this->ktadmin->curlPostChat($this->url.'?access_token='.$this->ktadmin->access_token,$postData, $callback);
        return $this;
    }
     /**
     * 解析回复消息
     */
    public function parseData($data)
    {
        //一次性返回数据
        if(json_decode($data) && @json_decode($data)->result){
            return json_decode($data)->result;
        }


        //流式数据
        $data = str_replace('data: {', '{', $data);
        $data = @json_decode($data, true);
        if (!is_array($data)) {
            return '';
        }
        if ($data['is_end'] == true) {
            return $data['result']."\n".'data: [DONE]';
        }
        return $data['result'] ?? '';


    }


}