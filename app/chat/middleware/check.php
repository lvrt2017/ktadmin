<?php

declare (strict_types = 1);

namespace app\chat\middleware;
use think\facade\Db;
use think\facade\Cookie;

class check
{
    protected $whiteList = [
        '/',
    ];
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */

    public function handle($request, \Closure $next)
    { 

        $verify = Cookie::get('verify',0);
        if(!$verify) return error('未登录');
        return $next($request);       
    }
}
