<?php

use think\facade\Route;

Route::group('index',function(){
	Route::any('getgroupchatmsg', 'Index/getGroupChatMsg');
	Route::any('getgrouplist', 'Index/getGroupList');
	Route::any('addgroup', 'Index/addGroup');
	Route::any('editgroup', 'Index/editGroup');
	Route::any('sendtext', 'Index/sendtext');
})->middleware(app\chat\middleware\check::class);